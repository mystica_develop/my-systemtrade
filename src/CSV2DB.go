package main

import (
	"encoding/csv"
	"golang.org/x/text/encoding/japanese"
	"golang.org/x/text/transform"
	"io"
	"os"
)

func failOnError(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	csv2db("src/data/xm/EURJPY/EURJPY_Daily_201902010000_201904190000.csv", "D")
}
func csv2db(filename string, granularity string) {
	/*
		db := db.GetInstance()

		rows := readCsvFile(filename)
		for _, row := range rows {
			i := model.Instrument{
				Instrument:  resp.Instrument,
				Granularity: granularity,
				Time:        candlestick.Time,
				Volume:      candlestick.Volume,
				Open:        o,
				Close:       c,
				High:        h,
				Low:         l,
				Type:        candleType,
				Complete:    candlestick.Complete,
			}
			db.InsertInstrument(&i)
		}
	*/
}
func readCsvFile(filename string) [][]string {
	//読み込みファイル準備
	file1, err := os.Open(filename)
	failOnError(err)
	defer file1.Close()

	reader := csv.NewReader(transform.NewReader(file1, japanese.ShiftJIS.NewDecoder()))
	reader.LazyQuotes = true // ダブルクオートを厳密にチェックしない
	reader.Comma = '\t'

	row := [][]string{}

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else {
			failOnError(err)
		}
		var new_record []string
		for i, v := range record {
			if i > 0 {
				new_record = append(new_record, v)
			}
			row = append(row, new_record)

		}
	}
	return row
}
