package main

import (
	"api"
	"db"
	"fmt"
	"service/technicalindicator"
	"time"
)

func main() {
	db := db.GetInstance()

	api := &api.Api{}
	api.New()
	resp, _ := api.GetTrades()
	for _, trade := range resp.Trades {
		fmt.Printf("%s %s %s %s %s %s\n", trade.Id, trade.Instrument, trade.State, trade.CurrentUnits, trade.Price, trade.UnrealizedPL)

	}
	//	api.PostOrder("USD_JPY",100.00,1110,100.10,99.0)

	instrument := "USD_JPY"
	granularity := "M5"
	rsinum := 10
	smanum := 14
	emanum := 14
	macdsortterm := 12
	macdlongterm := 26
	macdsignalnum := 9


	/**
	ATR
	ボラリティーの強い場合、大きく動くので売買する

	Highs/Lows
	損切りラインを設定する

	トレンド系
	SMA/EMA
	Bull Bear Power
	トレンドを把握して、売りか買いかを判断する

	買い・売りシグナルで売買開始
	オシレーター系
	MACD
	ROC
	RSI
	STOCH
	RCI
	CCI

	複数の売り・書いによって売買を行う。シグナルが変化したら手仕舞い。（中立になったら）

	 */
	// SMA
	sma := technicalindicator.SMAIndicatorCalculation(instrument, granularity, time.Now(), rsinum)
	fmt.Printf("SMA(%d):%3.3f\n", smanum, sma)

	// EMA
	ema := technicalindicator.EMAIndicatorCalculation(instrument, granularity, time.Now(), emanum)
	fmt.Printf("EMA(%d):%3.3f\n", emanum, ema)

	// RSI
	rsi := technicalindicator.RSIIndicatorCalculation(instrument, granularity, time.Now(), rsinum)
	fmt.Printf("RSI(%d):%3.3f\n", rsinum, rsi)

	// MACD計算
	macd := technicalindicator.MACDIndicatorCalculation(instrument, granularity, time.Now(), macdsortterm, macdlongterm)
	// MACDシグナル計算
	macdsignal := technicalindicator.MACDIndicatorSignalCalculation(instrument, granularity, time.Now(), macdsortterm, macdlongterm, macdsignalnum)
	fmt.Printf("MACD(%d,%d,%d):%3.3f Signal:%3.3f\n", macdsortterm, macdlongterm, macdsignalnum, macd, macdsignal)


	// STOCH
	stochnum := 14
	stoch := technicalindicator.STOCHIndicatorCalculation(instrument, granularity, time.Now(), stochnum)
	fmt.Printf("STOCH(%d):%3.3f\n", stochnum, stoch)

	// CCI
	ccinum := 14
	cci := technicalindicator.CCIIndicatorCalculation(instrument, granularity, time.Now(), ccinum)
	fmt.Printf("CCI(%d):%3.3f\n", ccinum, cci)

	// ATR
	atrnum := 14
	atr := technicalindicator.ATRIndicatorCalculation(instrument, granularity, time.Now(), atrnum)
	fmt.Printf("ATR(%d):%3.3f\n", atrnum, atr)

	// ROC(10日、20日、25日)
	roc10num := 10
	roc10 := technicalindicator.ROCIndicatorCalculation(instrument, granularity, time.Now(), roc10num)
	fmt.Printf("ROC(%d):%3.3f\n", roc10num, roc10)
	roc20num := 20
	roc20 := technicalindicator.ROCIndicatorCalculation(instrument, granularity, time.Now(), roc20num)
	fmt.Printf("ROC(%d):%3.3f\n", roc20num, roc20)
	roc25num := 25
	roc25 := technicalindicator.ROCIndicatorCalculation(instrument, granularity, time.Now(), roc25num)
	fmt.Printf("ROC(%d):%3.3f\n", roc25num, roc25)

	// Highs/Lows 移動平均線のトレンドと合わせて対応（上ならHighs突破で・・・とか）
	highlownum := 14
	high, low := technicalindicator.HighsLowsIndicatorCalculation(instrument, granularity, time.Now(), highlownum)
	fmt.Printf("Highs/Lows(%d):%3.3f %3.3f\n", highlownum, high, low)


	// Bull/Bear Power
	bullbearnums := 14
	bull, bear := technicalindicator.BullAndbearPowerIndicatorCalculation(instrument, granularity, time.Now(), bullbearnums)
	fmt.Printf("Bull/Bear Power(%d):%3.3f %3.3f\n", bullbearnums, bull, bear)


	// Ultimate Oscillator（むずかしいw）
	// DMI
	// ADX

	// 以下の判断方法は？要検討
	// 強気の乖離(ブリッシュ・ダイバージェンス)
	// 弱気の乖離(ベアリッシュ・ダイバージェンス)

	// 1.売買執行条件
	//  0.条件設定
	//  1.Sell
	//  2.Buy
	// 2.全てtrueで注文(成行）
	// 3.利確・損切り条件
	//  0.条件設定
	//  1.Sell
	//  2.Buy
	// 2.全てtrueで注文(成行）

	// 指標発表前には手仕舞い
	// 逆指値で利確・損切りを明確に

	defer db.Close()
}
