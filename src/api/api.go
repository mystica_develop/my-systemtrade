package api

import (
	"api/response"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Staging
const ValidToken = "37a7e934b754ae12e34051de378157f2-81abd033327fbc33d384597931f71906"
const URL = "https://api-fxpractice.oanda.com"
const ACCOUNT = "101-009-10659952-001"

// Production
/*
const ValidToken = "6c9b643197bb2862e65ccb2f2bfec31d-deb331e99ec10b3ddabae8aa9c6af8eb"
const URL = "https://api-fxtrade.oanda.com"
const ACCOUNT = "001-009-2646253-001"
*/

type Api struct {
	token      string
	httpclient *http.Client
}

// New builds a API client from the provided token and options.
func (api *Api) New() {
	client := &http.Client{Timeout: time.Duration(30) * time.Second}
	api.token = ValidToken
	api.httpclient = http.DefaultClient

	OptionHTTPClient(client)(api)
}

type Option func(*Api)

func OptionHTTPClient(c *http.Client) Option {
	return func(api *Api) {
		api.httpclient = c
	}
}

func (api *Api) Post(url string, jsonStr string) (*http.Response, error) {
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer([]byte(jsonStr)))
	if err != nil {
		return nil, err
	}

	// Content-Type 設定
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", api.token))

	resp, err := api.request(req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (api *Api) Get(url string) (*http.Response, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", api.token))

	//	fmt.Println(url)
	resp, err := api.request(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode >= 400 {
		return nil, fmt.Errorf("bad response status code %d", resp.StatusCode)
	}
	if err != nil {
		return nil, err
	}
	return resp, nil
}

// request .
func (api *Api) request(req *http.Request) (*http.Response, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 30000*time.Millisecond)
	defer cancel()

	req = req.WithContext(ctx)

	respCh := make(chan *http.Response)
	errCh := make(chan error)

	go func() {
		resp, err := api.httpclient.Do(req)
		if err != nil {
			errCh <- err
			return
		}

		respCh <- resp
	}()

	select {
	case resp := <-respCh:
		return resp, nil

	case err := <-errCh:
		return nil, err

	case <-ctx.Done():
		return nil, errors.New("HTTP request cancelled")
	}
}

type RoundTripFunc func(req *http.Request) *http.Response

func (f RoundTripFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

func (api *Api) GetAccounts() (*response.AccountResponseBody, error) {
	resp, _ := api.Get(URL + "/v3/accounts")

	defer resp.Body.Close()
	var r io.Reader = resp.Body

	var body response.AccountResponseBody
	if err := json.NewDecoder(r).Decode(&body); err != nil {
		return nil, err
	}
	return &body, nil
}

/*
 from = 開始 to = 終わり
 count = 数(max= 5000)
 price = M(固定)
 granularity = S5 M5 M15 M30 H1 H4 H8 D
*/
/*
// 足の数を指定した場合
	instrument := "EUR_USD"
	count := "100"
	granularity := "S5"
	resp, err := api.GetCandles(instrument, count, granularity)
	if err != nil {
		fmt.Println(err)
		return
	}
*/

func (api *Api) GetTrades() (*response.TradesResponseBody, error) {
	url := URL + "/v3/accounts/" + ACCOUNT + "/trades"
	resp, err := api.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var r io.Reader = resp.Body

	var body response.TradesResponseBody
	if err := json.NewDecoder(r).Decode(&body); err != nil {
		return nil, err
	}
	return &body, nil
}

func (api *Api) GetCandles(instrument string, price string, count string, granularity string) (*response.CandlestickResponseBody, error) {
	url := URL + "/v3/instruments/{INSTRUMENT}/candles?count={COUNT}&price={PRICE}&granularity={GRANULARITY}"
	url = strings.Replace(url, "{INSTRUMENT}", instrument, -1)
	url = strings.Replace(url, "{COUNT}", count, -1)
	url = strings.Replace(url, "{PRICE}", price, -1)
	url = strings.Replace(url, "{GRANULARITY}", granularity, -1)

	resp, err := api.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var r io.Reader = resp.Body
	//	r = io.TeeReader(r, os.Stderr)

	var body response.CandlestickResponseBody
	if err := json.NewDecoder(r).Decode(&body); err != nil {
		return nil, err
	}
	return &body, nil
}

/*
{"order": {"price": "1.5000","stopLossOnFill": { "timeInForce": "GTC", "price": "1.7000"},"takeProfitOnFill": {"price": "1.14530"},"timeInForce": "GTC","instrument": "USD_CAD","units": "-1000","type": "LIMIT","positionFill": "DEFAULT"}}
*/
func (api *Api) PostOrder(instrument string, price float64, units int, takeProfitOnFill float64, stopLossOnFill float64) error {
	b := `{"order": {"price": "` + strconv.FormatFloat(price, 'f', 5, 64) + `","stopLossOnFill": { "timeInForce": "GTC", "price": "` + strconv.FormatFloat(stopLossOnFill, 'f', 5, 64) + `"},"takeProfitOnFill": {"price": "` + strconv.FormatFloat(takeProfitOnFill, 'f', 5, 64) + `"},"timeInForce": "GTC","instrument": "` + instrument + `","units": "` + strconv.Itoa(units) + `","type": "LIMIT","positionFill": "DEFAULT"}}`
	url := URL + "/v3/accounts/" + ACCOUNT + "/orders"
	resp, err := api.Post(url, b)

	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}

func (api *Api) GetCandles2(instrument string, price string, granularity string, from string, to string) (*response.CandlestickResponseBody, error) {
	url := URL + "/v3/instruments/{INSTRUMENT}/candles?from={FROM}&to={TO}&price={PRICE}&granularity={GRANULARITY}"
	url = strings.Replace(url, "{INSTRUMENT}", instrument, -1)
	url = strings.Replace(url, "{FROM}", from, -1)
	url = strings.Replace(url, "{TO}", to, -1)
	url = strings.Replace(url, "{PRICE}", price, -1)
	url = strings.Replace(url, "{GRANULARITY}", granularity, -1)

	var resp, err = api.Get(url)

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var r io.Reader = resp.Body
	//	r = io.TeeReader(r, os.Stderr)

	var body response.CandlestickResponseBody
	if err := json.NewDecoder(r).Decode(&body); err != nil {
		return nil, err
	}
	return &body, nil
}
