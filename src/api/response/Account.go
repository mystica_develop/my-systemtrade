package response

type AccountResponseBody struct {
	Accounts []Account `json:"accounts"`
}

type Account struct {
	Id string `json:"id"`
}