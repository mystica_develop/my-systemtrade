package response

import "time"

type TradesResponseBody struct {
	Trades []Trade `json:"trades"`
}

type Trade struct {
	Id string `json:"id"`
	Instrument string `json:"instrument"`
	Price string `json:"price"`
	OpenTime time.Time `json:"openTime"`
	InitialUnits string `json:"initialUnits"`
	InitialMarginRequired string `json:"initialMarginRequired"`
	State string `json:"state"`
	CurrentUnits string `json:"currentUnits"`
	RealizedPL string `json:"realizedPL"`
	Financing string `json:"financing"`
	UnrealizedPL string `json:"unrealizedPL"`
	MarginUsed string `json:"marginUsed"`
}