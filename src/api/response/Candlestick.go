package response

import "time"

type CandlestickResponseBody struct {
	Instrument string `json:instrument`
	Granularity string `json:granularity`
	Candlestick []Candlestick `json:"candles"`
}

/*
complete": true,
"volume": 1,
"time": "2019-03-22T20:31:20.000000000Z",
"mid":{"o": "1.12970", "h": "1.12970", "l": "1.12970", "c": "1.12970"…}
 */
type Candlestick struct {
	Complete bool `json:"complete"`
	Volume int `json:"volume"`
	Time time.Time `json:"time"`
	Mid CandlestickData `json:"mid"`
}

type CandlestickData struct {
	Open string `json:"o"`
	Close string `json:"c"`
	High string `json:"h"`
	Low string `json:"l"`
}