package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"service"
	"time"
)


func main() {
	fmt.Println("処理開始")
	start := time.Date(2010, 1, 1, 0, 0, 0, 0, time.Local)
	end := time.Now()

	//	instruments := []string{"USD_JPY", "EUR_JPY", "EUR_USD"}
	instruments := []string{"USD_JPY"}

	c := make(chan int, len(instruments))
	for _, instrument := range instruments {
		service := &service.BackTest2Service{Instrument: instrument, Granularity:"M5"}
		go service.Execute(start, end, c)
	}
	result02, result01 := <-c, <-c
	fmt.Println("Main:", result01)
	fmt.Println("Main:", result02)
	fmt.Println("処理終了")
}