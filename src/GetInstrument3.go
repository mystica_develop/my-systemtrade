package main

import (
	"db"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"service"
	"time"
)

func main() {
	fmt.Println("処理開始")

	//	instruments := []string{"USD_JPY", "EUR_JPY", "EUR_USD"}
	instruments := []string{"EUR_JPY"}
	granularities := []string{"M5", "M15", "M30", "H1", "H4", "H8"}
	prices := []string{"M"}

	c := make(chan int, len(instruments)*len(granularities)*len(prices))
	for _, instrument := range instruments {
		for _, granularity := range granularities {
			for _, price := range prices {
				start := time.Date(2008, 1, 1, 0, 0, 0, 0, time.Local)
				end := time.Now()

				service := &service.GetInstumentService{Instrument: instrument, Price: price, Granularity: granularity}
				if granularity == "S5" {
					go service.Execute(start, end, 45, c)
				} else if granularity == "S15" {
					go service.Execute(start, end, 180, c)
				} else if granularity == "S30" {
					go service.Execute(start, end, 360, c)
				} else {
					go service.Execute(start, end, 60*24*1, c)
				}
			}
		}
		db := db.GetInstance()
		db.Close()
	}
}
