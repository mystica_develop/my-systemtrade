package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `{
	"asset": [
	{
		"price": 100000000
	}
],
"profit_and_loss": [
{
"unrealized_gain": 500000,
"daily": 100000,
"monthly": 200000,
"annual": 300000
}
],
"open_interest": [
{
"code": "JPY/USD",
"quantity": 1000,
"pips": 10,
"price": 100000,
"status": "available"
},
{
"code": "XXX/YYY",
"quantity": 2000,
"pips": 20,
"price": 200000,
"status": "on order"
}
]
}`)
}

func log(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, `{
		"log": [
	{
		"level": "info",
		"message": "[注文]******",
		"created_at": "2019/4/9 12:00:00",
		"process": "売買"
	},
	{
		"level": "info",
		"message": "[注文]******",
		"created_at": "2019/4/9 12:00:00",
		"process": "売買"
	},
	{
		"level": "info",
		"message": "[注文]******",
		"created_at": "2019/4/9 12:00:00",
		"process": "売買"
	},
	{
		"level": "info",
		"message": "[注文]******",
		"created_at": "2019/4/9 12:00:00",
		"process": "売買"
	},
]
}
`)
}

func notification(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, `{
		"notification": [
	{
		"title": "タイトル１",
		"content": "これは１件目のメッセージです。",
		"created_at": "2019/4/9 12:00:00"
	},
		{
			"title": "タイトル２",
			"content": "これは２件目のメッセージです。",
			"created_at": "2019/4/9 13:00:00"
		},
		{
			"title": "タイトル３",
			"content": "これは３件目のメッセージです。",
			"created_at": "2019/4/9 14:00:00"
		}
]
}
`)
}
func candles(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `{ 
      "chart" :[
             {
                     "open": 11,
                     "high": 11,
                     "low":18,
                     "close":13,
                     "label": "2019/04/05 19:00"
              },
             {
                     "open": 28,
                     "high": 20,
                     "low":15,
                     "close":17,
                     "label": "2019/04/05 19:05"
              },
             {
                     "open": 26,
                     "high": 30,
                     "low":18,
                     "close":20,
                     "label": "2019/04/05 19:10"
              }
       ]
}
`)
}

func main() {
	http.HandleFunc("/", handler) // ハンドラを登録してウェブページを表示させる
	http.HandleFunc("/candles", candles) // ハンドラを登録してウェブページを表示させる
	http.HandleFunc("/log", log) // ハンドラを登録してウェブページを表示させる
	http.HandleFunc("/notification", notification) // ハンドラを登録してウェブページを表示させる
	http.ListenAndServe(":8080", nil)
}