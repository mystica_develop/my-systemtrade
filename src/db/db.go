package db

import (
	"database/sql"
	"db/model"
	_ "db/model"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

var instance *DBInfo

type DBInfo struct {
	connection                 *sql.DB
	stmt                       *sql.Stmt
	stmtIns                    *sql.Stmt
	stmtDel                    *sql.Stmt
	stmtSelect                 *sql.Stmt
	stmtSelectOrder            *sql.Stmt
	stmtSelect2                *sql.Stmt
	stmtSelectInstrumentById   *sql.Stmt
	stmtUpdateInstrument       *sql.Stmt
	stmtUpdateInstrumentByTime *sql.Stmt
}

func GetInstance() *DBInfo {
	if instance == nil {
		instance = &DBInfo{}
		instance.Connect()
	}
	return instance
}
func (db *DBInfo) Connect() {
	cnn, _ := sql.Open("mysql", "root@tcp(localhost:3306)/systemtrade?parseTime=true")
	db.connection = cnn

	// SELECT * FROM instruments WHERE instrument = "EUR_USD" AND granularity = "M5" AND time > "2017-01-01" AND time < "2017-01-03";;
	stmtSelect, err := db.connection.Prepare("SELECT * FROM instruments WHERE instrument = ? AND granularity = ? AND time > ? AND time < ? ORDER BY time")
	if err != nil {
		panic(err.Error())
	}
	db.stmtSelect = stmtSelect

	// SELECT * FROM instruments WHERE instrument = "EUR_USD" AND granularity = "M5" AND time > "2017-01-01" AND time < "2017-01-03";;
	stmtSelectOrder, err := db.connection.Prepare("SELECT * FROM (SELECT * FROM instruments WHERE instrument = ? AND granularity = ? ORDER BY time desc LIMIT 120) as A ORDER BY time")
	if err != nil {
		panic(err.Error())
	}
	db.stmtSelectOrder = stmtSelectOrder

	stmtSelectInstrumentById, err := db.connection.Prepare("SELECT * FROM instruments WHERE id = ? ORDER BY time")
	if err != nil {
		panic(err.Error())
	}
	db.stmtSelectInstrumentById = stmtSelectInstrumentById

	stmtSelect2, err := db.connection.Prepare("SELECT count(*) FROM instruments WHERE instrument = ? AND granularity = ? AND time = ? ORDER BY time")
	if err != nil {
		panic(err.Error())
	}
	db.stmtSelect2 = stmtSelect2

	stmtDel, err := db.connection.Prepare("DELETE FROM instruments WHERE instrument = ? AND granularity = ? AND time = ?")
	if err != nil {
		panic(err.Error())
	}
	db.stmtDel = stmtDel

	stmtUpdateInstrument, err := db.connection.Prepare("UPDATE instruments SET order_complete = ? WHERE id = ?")
	if err != nil {
		panic(err.Error())
	}
	db.stmtUpdateInstrument = stmtUpdateInstrument

	stmtUpdateInstrumentByTime, err := db.connection.Prepare("UPDATE instruments SET volume = ?, open = ?, close = ?, low = ?, high = ?, type = ?, complete = ? WHERE instrument = ? AND granularity = ? AND time = ?")
	if err != nil {
		panic(err.Error())
	}
	db.stmtUpdateInstrumentByTime = stmtUpdateInstrumentByTime
}

func (db *DBInfo) Close() {
	defer db.stmt.Close()
//	defer db.stmtIns.Close()
	defer db.stmtUpdateInstrument.Close()
	defer db.stmtUpdateInstrumentByTime.Close()
	defer db.stmtDel.Close()
	defer db.stmtSelect.Close()
	defer db.stmtSelectOrder.Close()
	defer db.stmtSelect2.Close()
	defer db.stmtSelectInstrumentById.Close()
	defer db.connection.Close()
}

func (db *DBInfo) SelectInstrumentOrder(instrument string, granularity string) []model.Instrument {
	rows, err := db.stmtSelectOrder.Query(instrument, granularity)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	s := []model.Instrument{}
	for rows.Next() {
		model := model.Instrument{}
		model.New(rows)
		s = append(s, model)
	}
	return s
}

func (db *DBInfo) SelectInstrument(instrument string, granularity string, time time.Time, limit int) []model.Instrument {

	if db.stmt == (*sql.Stmt)(nil) {
		stmt, err := db.connection.Prepare("SELECT * FROM (SELECT * FROM instruments WHERE instrument = ? AND granularity = ? AND time <= ? ORDER BY time DESC LIMIT ?) as A ORDER BY time ")
		if err != nil {
			panic(err.Error())
		}
		db.stmt = stmt
	}

	rows, err := db.stmt.Query(instrument, granularity, time, limit)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	s := []model.Instrument{}
	for rows.Next() {
		model := model.Instrument{}
		model.New(rows)
		s = append(s, model)
	}
	return s
}

func (db *DBInfo) SelectInstrumentBacktest(instrument string, granularity string, from time.Time, to time.Time) []model.Instrument {
	rows, err := db.stmtSelect.Query(instrument, granularity, from, to)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	s := []model.Instrument{}
	for rows.Next() {
		model := model.Instrument{}
		model.New(rows)
		s = append(s, model)
	}
	return s
}

func (db *DBInfo) SelectInstrumentById(id int64) *model.Instrument {
	rows, err := db.stmtSelect.Query(id)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	model := model.Instrument{}
	for rows.Next() {
		model.New(rows)
	}
	return &model
}

func (db *DBInfo) ExistInstrumentMatchCandlestick(instrument string, granularity string, time time.Time) bool {
	rows, err := db.stmtSelect2.Query(instrument, granularity, time)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	count := 0
	for rows.Next() {
		rows.Scan(&count)
		if count == 0 {
			return false
		}
	}
	return true
}

func (db *DBInfo) UpdateInstrument(id int, orderComplete bool) {
	// insert into instruments (instrument, granularity, time, volume, open, close, low, high, type, complete, created_at, updated_at, deleted_at) values ("TEST", "granularity 1", now(), 1, 123.456, 123.456, 123.456, 123.456, 0, 1, now(), now(), null);
	_, err := db.stmtUpdateInstrument.Exec(orderComplete, id)
	if err != nil {
		panic(err.Error())
	}
}

func (db *DBInfo) UpdateInstrumentByTime(instrument *model.Instrument) {
	_, err := db.stmtUpdateInstrumentByTime.Exec(
		instrument.Volume, instrument.Open, instrument.Close, instrument.Low, instrument.High, instrument.Type, instrument.Complete, instrument.Instrument, instrument.Granularity, instrument.Time)
	if err != nil {
		panic(err.Error())
	}
}

func (db *DBInfo) DeleteInstrument(instrument string, granularity string, time time.Time) {
	// insert into instruments (instrument, granularity, time, volume, open, close, low, high, type, complete, created_at, updated_at, deleted_at) values ("TEST", "granularity 1", now(), 1, 123.456, 123.456, 123.456, 123.456, 0, 1, now(), now(), null);
	_, err := db.stmtDel.Exec(instrument, granularity, time)
	if err != nil {
		panic(err.Error())
	}
}

func (db *DBInfo) InsertInstrument(instrument *model.Instrument) {

	stmtIns, err := db.connection.Prepare("insert into instruments (instrument, granularity, time, volume, open, close, low, high, type, complete) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	defer stmtIns.Close()
	if err != nil {
		panic(err.Error())
	}

	// insert into instruments (instrument, granularity, time, volume, open, close, low, high, type, complete, created_at, updated_at, deleted_at) values ("TEST", "granularity 1", now(), 1, 123.456, 123.456, 123.456, 123.456, 0, 1, now(), now(), null);
	_, err = stmtIns.Exec(
		instrument.Instrument, instrument.Granularity, instrument.Time, instrument.Volume, instrument.Open, instrument.Close, instrument.Low, instrument.High, instrument.Type, instrument.Complete)
	if err != nil {
		panic(err.Error())
	}
}

/*
func (db *DBInfo) InsertTrade(trade *model.Trade) {
	// insert into instruments (instrument, granularity, time, volume, open, close, low, high, type, complete, created_at, updated_at, deleted_at) values ("TEST", "granularity 1", now(), 1, 123.456, 123.456, 123.456, 123.456, 0, 1, now(), now(), null);
	_, err := db.stmtIns.Exec(
		instrument.Instrument, instrument.Granularity, instrument.Time, instrument.Volume, instrument.Open, instrument.Close, instrument.Low, instrument.High, instrument.Type, instrument.Complete)
	if err != nil {
		panic(err.Error())
	}
}
*/
