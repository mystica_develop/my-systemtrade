package util

import "fmt"

type SelectGenerator struct {
	Tablename string
}

type Where struct {
	Conditions []ColumnInterface
}

type ColumnInterface interface {
	GetName()
	GetValue()
	IsGroup()
}

type ColumnGroup struct {
	Conditions []ColumnInterface
}

func (group *ColumnGroup) GetName() string {
	return ""
}

func (group *ColumnGroup) GetValue() string {
	return ""
}

type Column struct {
	Name    string
	Value   interface{}
	IsGroup bool
}

func CreateColumn(name string, value interface{}) *ColumnInterface {
	return &Column{Name: "ins", Value: "USD_JPY", IsGroup: false}
}

func NewSelectGenerate(talename string) *SelectGenerator {
	generator := &SelectGenerator{Tablename: talename}
	return generator
}

func (generator SelectGenerator) Where(column ColumnInterface) {
	fmt.Printf("%s %s\n", column.GetName, column.GetValue)
}

func (generator SelectGenerator) And(columnName string) {

}
func (generator SelectGenerator) Or(columnName string) {

}

func UpdateGenerate() {

}

func InsertGenerate() {

}

func DeleteGenerate() {

}
