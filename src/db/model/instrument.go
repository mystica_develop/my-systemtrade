package model

import (
	"database/sql"
	"time"
)

/*
DROP TABLE instruments;
CREATE TABLE  IF NOT EXISTS instruments(
	id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	instrument varchar(32) NOT NULL,
 	granularity varchar(16) NOT NULL,
	time TIMESTAMP NOT NULL,
	volume int NOT NULL,
	open float NOT NULL,
	low float NOT NULL,
	high float NOT NULL,
	close float NOT NULL,
	type int NOT NULL DEFAULT 0,
	complete tinyint(1) NOT NULL DEFAULT false,
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	deleted_at TIMESTAMP
);
ALTER TABLE instruments ADD INDEX index_posts_on_instrument_granularity_time(instrument, granularity, time);
alter table instruments add order_complete int;


INSERT INTO instruments (instrument, granularity, time, volume, open, close, low, high, type, complete, created_at, updated_at, deleted_at) values ("TEST", "granularity 1", now(), 1, 123.456, 123.456, 123.456, 123.456, 0, 1, now(), now(), null);
*/
type Instrument struct {
	Id             int
	Instrument     string
	Granularity    string
	Time           time.Time
	Volume         int
	Open           float64
	Low            float64
	High           float64
	Close          float64
	Type           int
	Complete       bool
	CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      time.Time
	OrderComplete  bool
	OrderComplete2 bool
}

func (model *Instrument) New(row *sql.Rows) {
	row.Scan(
		&model.Id,
		&model.Instrument,
		&model.Granularity,
		&model.Time,
		&model.Volume,
		&model.Open,
		&model.Low,
		&model.High,
		&model.Close,
		&model.Type,
		&model.Complete,
		&model.CreatedAt,
		&model.UpdatedAt,
		&model.DeletedAt,
		&model.OrderComplete,
	)
}
