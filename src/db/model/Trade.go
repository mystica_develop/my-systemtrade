package model

import (
	"database/sql"
	"time"
)

/*
DROP TABLE trades;
CREATE TABLE  IF NOT EXISTS trades(
	id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	sysId INT NOT NULL,
	instrument varchar(32) NOT NULL,
	price varchar(32) NOT NULL,
	opentime TIMESTAMP NOT NULL,
	initial_units int NOT NULL,
	initial_margin_required float NOT NULL,
	state varchar NOT NULL,
	current_units int NOT NULL
	realized_pl float NOT NULL,
	financing float NOT NULL,
	unrealized_pl float NOT NULL,
	margin_used float NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	deleted_at TIMESTAMP DEFAULT NULL
);
ALTER TABLE instruments ADD INDEX index_posts_on_instrument_granularity_time(instrument, granularity, time);
 */
type Trade struct {
	Id int

	SysId int
	Instrument string
	Price float64
	Opentime time.Time
	InitialUnits int
	InitialMarginRequired float64
	State string
	CurrentUnits int
	RealizedPL float64
	Financing float64
	UnrealizedPL float64
	MarginUsed float64

	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

func (model *Trade) New(row *sql.Rows) {
	row.Scan(
		&model.Id,
		&model.SysId,
		&model.Instrument,
		&model.Price,
		&model.Opentime,
		&model.InitialUnits,
		&model.InitialMarginRequired,
		&model.State,
		&model.CurrentUnits,
		&model.RealizedPL,
		&model.Financing,
		&model.UnrealizedPL,
		&model.MarginUsed,
		&model.CreatedAt,
		&model.UpdatedAt,
		&model.DeletedAt,
	)
}
