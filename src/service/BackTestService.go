package service

import (
	"db"
	"db/model"
	"fmt"
	"time"
)

type BackTestService struct {
	Instrument string
}


type BackTestInfo struct {
	Status int

	ProfitOrderPips float64    // 利確
	LossCutOrderPips float64   // 損切り

	CandleType int
	CandleTypeCount int

	OrderType int
	OrderResetCount int
	ContractOrderStatus bool

	OrderPrice float64
	ProfitOrderPrice float64
	LossCutOrderPrice float64


	Pips float64
	ProfitCount int
	LossCutCount int
	CancelCount int
}

func (info *BackTestInfo) Reset() {
	info.CandleType = 0
	info.CandleTypeCount = 0

	info.OrderType = 0
	info.OrderResetCount = 0
	info.ContractOrderStatus = false

	info.OrderPrice = 0
	info.ProfitOrderPrice = 0
	info.LossCutOrderPrice = 0

	info.Status = 0
}

func (info *BackTestInfo) Cals(instrument model.Instrument) {
	i := info.Status
	switch i {
	case 0:
		if instrument.Type == info.CandleType && info.CandleType != 0 {
			info.CandleTypeCount++;
		} else {
			info.CandleType = instrument.Type
			info.CandleTypeCount = 0
		}
		if info.IsOrder(instrument) {
			info.Order(instrument)
		}
		break
	case 1:
		if info.IsContract(instrument) {
			info.Contract(instrument)
		}
		break
	case 2:
		if info.IsLossCut(instrument) {
			info.LossCut(instrument)
		} else if info.IsProfit(instrument) {
			info.Profit(instrument)
		}
		break
	}
}

func (info *BackTestInfo) IsOrder(instrument model.Instrument) bool{
	if (info.CandleTypeCount >= 3) {
		return true
	}
	return false
}

func (info *BackTestInfo) Order(instrument model.Instrument){
	if (info.CandleType == 1) {

		info.OrderType = 1
		info.OrderPrice = instrument.Low
		info.ProfitOrderPrice = instrument.Low + info.ProfitOrderPips
		info.LossCutOrderPrice = instrument.Low - info.LossCutOrderPips
		info.Status = 1
		info.CandleTypeCount = 0

		fmt.Printf("[%s] [%s] 買い注文(注文：%5.3f 利確：%5.3f 損切り：%5.3f )\n", instrument.Time, instrument.Instrument, info.OrderPrice, info.ProfitOrderPrice, info.LossCutOrderPrice)
	} else if (info.CandleType == -1) {
		info.OrderType = -1
		info.OrderPrice = instrument.High
		info.ProfitOrderPrice = instrument.High - info.ProfitOrderPips
		info.LossCutOrderPrice = instrument.High + info.LossCutOrderPips
		info.Status = 1
		info.CandleTypeCount = 0

		fmt.Printf("[%s] [%s] 売り注文(注文：%5.3f 利確：%5.3f 損切り：%5.3f )\n", instrument.Time, instrument.Instrument, info.OrderPrice, info.ProfitOrderPrice, info.LossCutOrderPrice)
	} else {
		info.Reset()
	}
}
func (info *BackTestInfo) IsContract(instrument model.Instrument) bool{
	if (info.OrderType == 1) {
		if (info.OrderPrice < instrument.Low) {
			return true
		} else if (info.OrderResetCount >= 12) {
			fmt.Printf("[%s] [%s] 注文キャンセル(注文：%5.3f 利確：%5.3f 損切り：%5.3f )\n", instrument.Time, instrument.Instrument, info.OrderPrice, info.ProfitOrderPrice, info.LossCutOrderPrice)
			info.CancelCount ++
			info.Reset()
		} else {
			info.OrderResetCount ++
		}
	} else if (info.OrderType == -1) {
		if(info.OrderPrice < instrument.High) {
			return true
		} else if (info.OrderResetCount >= 12) {
			fmt.Printf("[%s] [%s] 注文キャンセル(注文：%5.3f 利確：%5.3f 損切り：%5.3f )\n", instrument.Time, instrument.Instrument, info.OrderPrice, info.ProfitOrderPrice, info.LossCutOrderPrice)
			info.CancelCount ++
			info.Reset()
		} else {
			info.OrderResetCount ++
		}
	}
	return false
}

func (info *BackTestInfo) Contract(instrument model.Instrument){
	if (info.OrderType == 1) {
		if(info.OrderPrice < instrument.Low) {
			fmt.Printf("[%s] [%s] 買い約定(注文：%5.3f 利確：%5.3f 損切り：%5.3f )\n", instrument.Time, instrument.Instrument, info.OrderPrice, info.ProfitOrderPrice, info.LossCutOrderPrice)
			info.ContractOrderStatus = true
			info.Status = 2
			info.OrderResetCount = 0
		}
	} else if (info.OrderType == -1) {
		if(info.OrderPrice < instrument.High) {
			fmt.Printf("[%s] [%s] 売り約定(注文：%5.3f 利確：%5.3f 損切り：%5.3f )\n", instrument.Time, instrument.Instrument, info.OrderPrice, info.ProfitOrderPrice, info.LossCutOrderPrice)
			info.ContractOrderStatus = true
			info.Status = 2
			info.OrderResetCount = 0
		}
	} else {
		fmt.Printf("[%s] [%s] ?????\n", instrument.Time, instrument.Instrument)
	}
}

func (info *BackTestInfo) IsLossCut(instrument model.Instrument) bool{
	if (info.OrderType == 1) {
		if (info.LossCutOrderPrice > instrument.Low) {
			return true
		}
	} else if (info.OrderType == -1) {
		if (info.LossCutOrderPrice < instrument.High) {
			return true
		}
	}
	return false
}

func (info *BackTestInfo) LossCut(instrument model.Instrument) {
	fmt.Printf("[%s] [%s] 損切り(注文：%5.3f)\n", instrument.Time, instrument.Instrument, info.LossCutOrderPrice)
	info.Pips -= info.LossCutOrderPips
	info.LossCutCount ++
	info.Reset()
}
func (info *BackTestInfo) IsProfit(instrument model.Instrument) bool{
	if (info.OrderType == 1) {
		if (info.ProfitOrderPrice < instrument.High) {
			return true
		}

	} else if (info.OrderType == -1) {
		if (info.ProfitOrderPrice > instrument.Low) {
			return true
		}
	}
	return false
}

func (info *BackTestInfo) Profit(instrument model.Instrument) {
	fmt.Printf("[%s] [%s] 利確(注文：%5.3f)\n", instrument.Time, instrument.Instrument, info.ProfitOrderPrice)
	info.Pips += info.ProfitOrderPips
	info.ProfitCount ++
	info.Reset()
}
func (service *BackTestService) Execute(start time.Time, end time.Time, c chan int) {


	weekday := start.Weekday()

	slide := 0
	switch weekday {
	case time.Monday:
		break
	case time.Tuesday:
		slide = -1
		break
	case time.Wednesday:
		slide = -2
		break
	case time.Thursday:
		slide = -3
		break
	case time.Friday:
		slide = -4
		break
	case time.Saturday:
		slide = -5
		break
	case time.Sunday:
		slide = -6
		break
	}
	start = start.Add(time.Duration(24*slide) * time.Hour)
	to := end

	db := db.GetInstance()

	// start ~ endまでのレコード全部取得
	instruments := db.SelectInstrumentBacktest(service.Instrument, "M5", start, to)

	info := &BackTestInfo{Status:0}
	info.ProfitOrderPips = 0.10
	info.LossCutOrderPips = 0.05
	for _, instrument := range instruments {
//		fmt.Printf("%s %s %s, %3.5f, %3.5f, %3.5f, %3.5f \n", instrument.Instrument, instrument.Granularity, instrument.Time, instrument.Open, instrument.Close, instrument.High, instrument.Low)
		fmt.Printf("%d", info.Status)
		info.Cals(instrument)
	}
	fmt.Printf("[%s] 処理件数[%d] \n", service.Instrument, len(instruments))
	fmt.Printf("[%s] 結果：[%5.3f] 利益確定回数：[%d] 損切り回数：[%d] 取消回数：[%d]\n", service.Instrument,  info.Pips, info.ProfitCount, info.LossCutCount, info.CancelCount)


		// 条件１
		// 陽線が３回続いたら
		// 陰線が３回続いたら
		// 50pipsで利確
		// 直近高値・底値で損切り

		// 条件２
		// サポートライン（高値・底値）を割ったら＊＊＊で注文
		// ＊＊＊は、直近ローソク足の高値・底値？
		// 100pipsで利確
		// 直近高値・底値で損切り

}

/*
 struct@
  条件判定＞Match＞注文
  条件判定＞Match＞約定
  条件判定＞Match＞決済
 */

// 安値・高値の判断
// 前回・高値（安値）中値を超えたら

// トレンドラインの判定

// 直近高値・底値の計算式

// 前回ローソク足の取得

