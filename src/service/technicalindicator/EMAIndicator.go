package technicalindicator

import (
	"db"
	"time"
)

func EMAIndicatorCalculation(instrument string, granularity string, time time.Time, nums int) float64 {
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, nums)

	sum := 0.0
	for _, record := range records {
		sum += record.Close
	}
	sum+=records[len(records)-1].Close

	return sum/(float64(nums)+1.0)
}
