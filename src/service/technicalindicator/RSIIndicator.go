package technicalindicator

import (
	"db"
	"math"
	"time"
)

func RSIIndicatorCalculation(instrument string, granularity string, time time.Time, nums int) float64 {
	// nums数分のレコードを取得する（timeから過去nums本）
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, nums)

	// 値上がりの合計pips
	increase := 0.0
	// 値下がりの合計pips
	drop := 0.0

	for _, record := range records {
		if record.Open < record.Close {
			increase -= RoundDown(record.Open - record.Close, 3)
		} else if record.Open > record.Close {
			drop += RoundDown(record.Open - record.Close, 3)
		}
	}
	increase = increase / float64(nums)
	drop = drop / float64(nums)

	return increase / (increase + drop) * 100.0
}

func RoundDown(num, places float64) float64 {
	shift := math.Pow(10, places)
	return math.Trunc(num*shift) / shift
}