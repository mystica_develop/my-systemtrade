package technicalindicator

import (
	"db"
	"time"
)

func ROCIndicatorCalculation(instrument string, granularity string, time time.Time, nums int) float64 {
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, nums)

	return (records[len(records)-1].Close - records[0].Close) / records[0].Close * 100
}