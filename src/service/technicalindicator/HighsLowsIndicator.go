package technicalindicator

import (
	"db"
	"time"
)

func HighsLowsIndicatorCalculation(instrument string, granularity string, time time.Time, nums int) (float64,float64) {
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, nums)

	high := 0.0
	low := 9999.0
	for _, record := range records {
		if (record.High > high) {high = record.High}
		if (record.Low < low) {low = record.Low}
	}

	return high, low
}