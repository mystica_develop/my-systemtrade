package technicalindicator

import (
	"db"
	"time"
)

func STOCHIndicatorCalculation(instrument string, granularity string, time time.Time, nums int) float64 {
	// nums数分のレコードを取得する（timeから過去nums本）
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, nums)

	// 最安値・最高値
	high, low := HighsLowsIndicatorCalculation(instrument, granularity, time, nums)
	sum := 0.0
	for _, record := range records {
		sum += (record.Close - low) / (high - low) * 100
	}

	return sum / float64(nums)
}
