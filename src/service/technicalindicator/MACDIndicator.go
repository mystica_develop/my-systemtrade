package technicalindicator

import (
	"db"
	"time"
)

func MACDIndicatorCalculation(instrument string, granularity string, time time.Time, macdshortterm int, macdlongterm int) float64 {
	// 短期EMA
	emashort := EMAIndicatorCalculation(instrument, granularity, time, macdshortterm)
	// 長期EMA
	emalong := EMAIndicatorCalculation(instrument, granularity, time, macdlongterm)
	return emashort-emalong
}


func MACDIndicatorSignalCalculation(instrument string, granularity string, time time.Time, macdshortterm int, macdlongterm int, macdsignal int) float64 {
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, macdsignal)
	sum := 0.0
	for _, record := range records {
		sum += MACDIndicatorCalculation(instrument, granularity, record.Time, macdshortterm, macdlongterm)
	}
	return sum / float64(macdsignal)
}

