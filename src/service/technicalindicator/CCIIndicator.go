package technicalindicator

import (
	"db"
	"time"
)

func CCIIndicatorCalculation(instrument string, granularity string, time time.Time, nums int) float64 {
	// nums数分のレコードを取得する（timeから過去nums本）
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, nums)

	// 最終脚の基準値
	lasttp := (records[len(records)-1].Low + records[len(records)-1].High +records[len(records)-1].Close) / 3.0
	// 代表値の単純移動平均線
	ma := 0.0
	for _, record := range records {
		ma += (record.High + record.Low + record.Close) / 3.0
	}
	ma = ma / float64(nums)

	sumd := 0.0
	for _, record := range records {
		sumd += lasttp - (record.High + record.Low + record.Close)
	}
	return (lasttp - ma) / (sumd / float64(nums) * 0.015) * 100
}
