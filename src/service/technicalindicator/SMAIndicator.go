package technicalindicator

import (
	"db"
	"time"
)

func SMAIndicatorCalculation(instrument string, granularity string, time time.Time, nums int) float64 {
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, nums)

	sum := 0.0
	for _, record := range records {
		sum += record.Close
	}

	return sum/(float64(nums))
}
