package technicalindicator

import (
	"db"
	"time"
)

func BullAndbearPowerIndicatorCalculation(instrument string, granularity string, time time.Time, nums int) (float64,float64) {
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, 1)
	ema := EMAIndicatorCalculation(instrument, granularity, time, nums)

	return records[0].High - ema, records[0].Low - ema
}