package technicalindicator

import (
	"db"
	"math"
	"time"
)

func ATRIndicatorCalculation(instrument string, granularity string, time time.Time, nums int) float64 {
	db := db.GetInstance()
	records := db.SelectInstrument(instrument, granularity, time, nums+1)

	sum := 0.0
	for i, record := range records {
		if (i==0){
			continue
		}
		hl := record.High - record.Low
		hc := record.High - records[i-1].Close
		cl := records[i-1].Close - record.Low
		sum += math.Max(math.Max(hl, hc), cl)
	}
	return sum / float64(nums)
}