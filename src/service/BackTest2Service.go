package service

import (
	"db"
	"fmt"
	"service/technicalindicator"
	"time"
)

type BackTest2Service struct {
	Instrument string
	Granularity string
}

func (service *BackTest2Service) Execute(start time.Time, end time.Time, c chan int) {


	weekday := start.Weekday()

	slide := 0
	switch weekday {
	case time.Monday:
		break
	case time.Tuesday:
		slide = -1
		break
	case time.Wednesday:
		slide = -2
		break
	case time.Thursday:
		slide = -3
		break
	case time.Friday:
		slide = -4
		break
	case time.Saturday:
		slide = -5
		break
	case time.Sunday:
		slide = -6
		break
	}
	start = start.Add(time.Duration(24*slide) * time.Hour)
	to := end

	db := db.GetInstance()

	// start ~ endまでのレコード全部取得
	instruments := db.SelectInstrumentBacktest(service.Instrument, "M5", start, to)

	info := &BackTestInfo{Status:0}
	info.ProfitOrderPips = 0.10
	info.LossCutOrderPips = 0.05
	for _, instrument := range instruments {
		if (service.ok(instrument.Time)) {
			if (service.isNow(instrument.Time)) {
				if (service.trend(instrument.Time) == 1) {
					fmt.Printf("[%s] [%s] [%3.5f]買い \n", service.Instrument, instrument.Time, instrument.Open)
				} else {
					fmt.Printf("[%s] [%s] [%3.5f]売り \n", service.Instrument, instrument.Time, instrument.Open)
				}
			}
		}
	}
	fmt.Printf("[%s] 処理件数[%d] \n", service.Instrument, len(instruments))
	fmt.Printf("[%s] 結果：[%5.3f] 利益確定回数：[%d] 損切り回数：[%d] 取消回数：[%d]\n", service.Instrument,  info.Pips, info.ProfitCount, info.LossCutCount, info.CancelCount)
}

func (service *BackTest2Service) ok(time time.Time) bool {
	// ATRが＊＊＊だったら
	/*
	atrnum := 14
	atr := technicalindicator.ATRIndicatorCalculation(service.Instrument, service.Granularity, time, atrnum)
	if (atr < 0.1) {
		fmt.Printf("[%s] [%s] [%3.3f] ぼらが低い\n", service.Instrument, time, atr)
		return false
	}
	*/
	return true
}

func (service *BackTest2Service) trend(time time.Time) int {
	shortnum := 10
	longnum := 40

	shortsma := technicalindicator.SMAIndicatorCalculation(service.Instrument, service.Granularity, time, shortnum)
	longsma := technicalindicator.SMAIndicatorCalculation(service.Instrument, service.Granularity, time, longnum)

	shortema := technicalindicator.EMAIndicatorCalculation(service.Instrument, service.Granularity, time, shortnum)
	longema := technicalindicator.EMAIndicatorCalculation(service.Instrument, service.Granularity, time, longnum)

	if (shortsma > longsma && shortema > longema) {
		return 1
	}

	if (shortsma < longsma && shortema < longema) {
		return -1
	}
	return 0
}

/**
全てのシグナルが点灯していれば書い
 */
func (service *BackTest2Service) isNow(time time.Time) bool {
	trend := service.trend(time)
	if (trend == 0) {
		return false
	}


	// MACD
	macdr := true
	macdsortterm := 12
	macdlongterm := 26
	macd := technicalindicator.MACDIndicatorCalculation(service.Instrument, service.Granularity, time, macdsortterm, macdlongterm)
	if macd == 0 {
		macdr = false
	}
	if macd < 0 && trend == 1 {
		macdr = false
	}
	if macd > 0 && trend == -1 {
		macdr = false
	}

	// ROC
	rocr := true
	roc10num := 10
	roc10 := technicalindicator.ROCIndicatorCalculation(service.Instrument, service.Granularity, time, roc10num)
	if roc10 == 0 {
		rocr = false
	}
	if roc10 < 0 && trend == 1 {
		rocr = false
	}
	if roc10 > 0 && trend == -1 {
		rocr = false
	}
	// RSI
	rsir := true
	rsinum := 10
	rsi := technicalindicator.RSIIndicatorCalculation(service.Instrument, service.Granularity, time, rsinum)
	if rsi > 30 && trend == 1 {
		rsir = false
	}
	if rsi < 70 && trend == -1 {
		rsir = false
	}
	// STOCH
	stochr := true
	stochnum := 14
	stoch := technicalindicator.STOCHIndicatorCalculation(service.Instrument, service.Granularity, time, stochnum)
	if stoch > 25 && trend == 1 {
		stochr = false
	}
	if stoch < 75 && trend == -1 {
		stochr = false
	}
	// CCI
	ccir := true
	ccinum := 14
	cci := technicalindicator.CCIIndicatorCalculation(service.Instrument, service.Granularity, time, ccinum)
	if cci > -100 && trend == 1 {
		ccir = false
	}
	if cci < 100 && trend == -1 {
		ccir = false
	}

	fmt.Printf("MACD:%t ROC:%t RSI:%t STOCH:%t CCI:%t\n", macdr, rocr, rsir, stochr, ccir)
	if (macdr && rocr && rsir && stochr && ccir) {
		return true
	}
	return false
}