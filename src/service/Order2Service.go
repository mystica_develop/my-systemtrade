package service

import (
	"db"
	"fmt"
	"log"
	"os"
)

type Order2Service struct {
	Instrument string
	Info OrderServiceInfo
}

func (service *Order2Service) Execute() {
	db := db.GetInstance()

	high := 0.0
	low := 1000.0

	// start ~ endまでのレコード全部取得
	instruments := db.SelectInstrumentOrder(service.Instrument, "M5")

	// 高値/底値を更新が止まったら、次の時間から逆値を計算する。を繰り返す
	// 移動平均線より上に行ったら高値として更新
	// 移動平均線より下に行ったら底値として更新

	// ５本移動平均線を定義
	// ２５本移動平均線を定義

	// １つの高値 ＆１つの底値の差が5pips以上であること
	// 底値・高値-1.0pipsで注文（移動平均線の状態により売り方買いかを判断）
	// 高値で

	for x := len(instruments) - 120; x < len(instruments); x++ {
		instrument := instruments[x]
		if (high < instrument.High) {
			high = instrument.High
		}
		if (low > instrument.Low) {
			low = instrument.Low
		}
	}
	avg25 := 0.0
	avg13 := 0.0
	avg5 := 0.0
	for y := len(instruments) - 25; y < len(instruments); y++ {
		avg25 += instruments[y].Open
	}
	for y := len(instruments) - 13; y < len(instruments); y++ {
		avg13 += instruments[y].Open
	}
	for y := len(instruments) - 5; y < len(instruments); y++ {
		avg5 += instruments[y].Open
	}

	avg5 = avg5 / 5
	avg13 = avg13 / 13
	avg25 = avg25 / 25

	if avg5 > avg13 {
	}

	if avg13 > avg25 {
	}

	str := fmt.Sprintf("[%s] 5:[%3.3f] 13:[%3.3f] 25:[%3.3f](%t)(%t)\n", service.Instrument, avg5, avg13, avg25, avg5 > avg13, avg13 > avg25)
	fmt.Printf(str)
	logwrite(service.Instrument, str)

//	fmt.Printf("[%s] 5：[%3.3f]　25：[%3.3f] [%t]\n", service.Instrument, avg5/5, avg25/25, );
//	fmt.Printf("[%s] 高値：[%3.3f]　底値：[%3.3f]\n", service.Instrument, high, low);
//	fmt.Printf("[%s] 現値：[%3.3f]\n", service.Instrument, now);

/*
	now := instruments[len(instruments)-1].Close
	idoheikinsenflag := (avg5 > avg13)
	if (high < low + 0.05) {
		str := fmt.Sprintf("[%s] 高値と底値が05.0pips以上離れていないので注文しない 高値：[%3.3f]　底値：[%3.3f] 5:[%3.3f] 25:[%3.3f]\n", service.Instrument, high, low, avg5, avg25)
		fmt.Printf(str)
		logwrite(service.Instrument, str)
	} else {
		if (idoheikinsenflag) {
			if (low + 0.01 >= now) {
				str := fmt.Sprintf("[%s] ロング注文しました [注文：%3.3f] [現値：%3.3f]  5:[%3.3f] 25:[%3.3f]\n", service.Instrument, low - 0.01, now, avg5, avg25)
				fmt.Printf(str)
				logwrite(service.Instrument, str)
			} else {
				str := fmt.Sprintf("[%s] 底値+0.01pips以下でないので ロング 注文しない [注文：%3.3f] [現値：%3.3f]  5:[%3.3f] 25:[%3.3f]\n", service.Instrument, low - 0.01, now, avg5, avg25)
				fmt.Printf(str)
				logwrite(service.Instrument, str)
			}
		} else {
			if (high - 0.01 <= now) {
				str := fmt.Sprintf("[%s] ショート注文しました [注文：%3.3f] [現値：%3.3f]  5:[%3.3f] 25:[%3.3f]\n", service.Instrument, high - 0.01, now, avg5, avg25)
				fmt.Printf(str)
				logwrite(service.Instrument, str)
			} else {
				str := fmt.Sprintf("[%s] 底値+0.01pips以下でないので ショート 注文しない [注文：%3.3f] [現値：%3.3f]  5:[%3.3f] 25:[%3.3f]\n", service.Instrument, high - 0.01, now, avg5, avg25)
				fmt.Printf(str)
				logwrite(service.Instrument, str)
			}
		}
	}
*/
}

func logwrite (instrument string, message string) {

	file, err := os.OpenFile(instrument + ".txt", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		//エラー処理
		log.Fatal(err)
	}
	defer file.Close()
	fmt.Fprint(file, message)
}