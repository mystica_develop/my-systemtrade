package service

import (
	"api"
	"db"
	"db/model"
	"fmt"
	"strings"
)

type OrderService struct {
	Instrument string
	Info OrderServiceInfo
}


type OrderServiceInfo struct {
	Status int
	OrderComplete bool

	ProfitOrderPips float64    // 利確
	LossCutOrderPips float64   // 損切り

	CandleType int
	CandleTypeCount int

	OrderType int
	OrderResetCount int
	ContractOrderStatus bool

	OrderPrice float64
	ProfitOrderPrice float64
	LossCutOrderPrice float64


	Pips float64
	ProfitCount int
	LossCutCount int
	CancelCount int
}

func (info *OrderServiceInfo) Reset() {
	info.CandleType = 0
	info.CandleTypeCount = 0

	info.OrderType = 0
	info.OrderResetCount = 0
	info.ContractOrderStatus = false

	info.OrderPrice = 0
	info.ProfitOrderPrice = 0
	info.LossCutOrderPrice = 0

	info.Status = 0
}

func (info *OrderServiceInfo) Cals(instrument model.Instrument) {
	i := info.Status
	switch i {
	case 0:
		if instrument.Type == info.CandleType && info.CandleType != 0 {
			info.CandleTypeCount++;
		} else {
			info.CandleType = instrument.Type
			info.CandleTypeCount = 0
		}
		if info.IsOrder(instrument) {
			info.Order(instrument)
			info.Reset()
		}
		break
	}
}

func (info *OrderServiceInfo) IsOrder(instrument model.Instrument) bool{
	if (info.CandleTypeCount >= 2) {
		return true
	}
	return false
}

func (info *OrderServiceInfo) Order(instrument model.Instrument){
	if (info.CandleType == 1) {

		info.OrderType = 1
		info.OrderPrice = instrument.Low
		info.ProfitOrderPrice = instrument.Low + info.ProfitOrderPips
		info.LossCutOrderPrice = instrument.Low - info.LossCutOrderPips
		info.Status = 1
		info.CandleTypeCount = 0

		api := &api.Api{}
		api.New()
		api.PostOrder(instrument.Instrument,info.OrderPrice,100, info.ProfitOrderPrice,info.LossCutOrderPrice )

		info.OrderComplete = true
		fmt.Printf("[%s] [%s] 買い注文(注文：%5.3f 利確：%5.3f 損切り：%5.3f )\n", instrument.Time, instrument.Instrument, info.OrderPrice, info.ProfitOrderPrice, info.LossCutOrderPrice)
	} else if (info.CandleType == -1) {
		info.OrderType = -1
		info.OrderPrice = instrument.High
		info.ProfitOrderPrice = instrument.High - info.ProfitOrderPips
		info.LossCutOrderPrice = instrument.High + info.LossCutOrderPips
		info.Status = 1
		info.CandleTypeCount = 0

		api := &api.Api{}
		api.New()
		api.PostOrder(instrument.Instrument,info.OrderPrice,-100, info.ProfitOrderPrice,info.LossCutOrderPrice )

		info.OrderComplete = true
		fmt.Printf("[%s] [%s] 売り注文(注文：%5.3f 利確：%5.3f 損切り：%5.3f )\n", instrument.Time, instrument.Instrument, info.OrderPrice, info.ProfitOrderPrice, info.LossCutOrderPrice)
	} else {
		info.Reset()
	}
}

func (service *OrderService) Execute() {

	db := db.GetInstance()

	// start ~ endまでのレコード全部取得
	instruments := db.SelectInstrumentOrder(service.Instrument, "M5")

	if (strings.HasSuffix(service.Instrument, "USD")) {
		service.Info.ProfitOrderPips = 0.0010
		service.Info.LossCutOrderPips = 0.0005
	} else if (strings.HasSuffix(service.Instrument, "JPY")) {
		service.Info.ProfitOrderPips = 0.10
		service.Info.LossCutOrderPips = 0.05
	} else {
		return
	}
	for i := 0; i < 3; i++ {
		instrument := instruments[i]
		if (instrument.OrderComplete) {
			fmt.Printf("注文済み \n")
			return
		}
	}
	for i := 0; i < 3; i++ {
		instrument := instruments[i]
		fmt.Printf("%s %s %s, %3.5f, %3.5f, %3.5f, %3.5f, %d \n", instrument.Instrument, instrument.Granularity, instrument.Time, instrument.Open, instrument.Low, instrument.High, instrument.Close, instrument.Type)
		service.Info.Cals(instrument)
	}
	if(service.Info.OrderComplete) {
		for i := 0; i < 3; i++ {
			model := instruments[i]
			db.UpdateInstrument(model.Id, true)
		}
	}
	fmt.Printf("%s \n", service.Info.CandleTypeCount)
}