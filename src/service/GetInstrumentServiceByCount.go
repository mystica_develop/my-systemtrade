package service

import (
	"api"
	"db"
	"db/model"
	"fmt"
	"strconv"
)

type GetInstumentServiceByCount struct {
	Instrument string
	Granularity string
}

func (service GetInstumentServiceByCount) Execute(count int) {
	api := &api.Api{}
	api.New()

	db := db.GetInstance()

	resp, err := api.GetCandles(service.Instrument, "M", strconv.Itoa(count), service.Granularity)
	if err != nil {
		fmt.Println(err)
		return
	}

//	fmt.Printf("%s %s %d 件の処理をおこないます。\n", service.Instrument, service.Granularity, len(resp.Candlestick))

	for i := 0; i < len(resp.Candlestick); i++ {
		candlestick := resp.Candlestick[i]

		// 存在してなければ行う
		exist := db.ExistInstrumentMatchCandlestick(resp.Instrument, resp.Granularity, candlestick.Time)
		o, _ := strconv.ParseFloat(candlestick.Mid.Open, 64)
		c, _ := strconv.ParseFloat(candlestick.Mid.Close, 64)
		h, _ := strconv.ParseFloat(candlestick.Mid.High, 64)
		l, _ := strconv.ParseFloat(candlestick.Mid.Low, 64)

		// 陽線・陰線判定
		candleType := 0
		if (o > c) {
			candleType = -1
		} else if (c > o) {
			candleType = 1
		}
		// ロウソク足をinsert
		i := model.Instrument{
			Instrument:  resp.Instrument,
			Granularity: resp.Granularity,
			Time:        candlestick.Time,
			Volume:      candlestick.Volume,
			Open:        o,
			Close:       c,
			High:        h,
			Low:         l,
			Type:        candleType,
			Complete:    candlestick.Complete,
		}

		if (exist) {
			db.UpdateInstrumentByTime(&i)
		} else {
			db.InsertInstrument(&i)
		}
	}
}