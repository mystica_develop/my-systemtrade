package service

import (
	"api"
	"db"
	"db/model"
	"fmt"
	"strconv"
	"time"
)

type GetInstumentService struct {
	Instrument  string
	Granularity string
	Price       string
}

func (service GetInstumentService) Execute(start time.Time, end time.Time, span float64, c chan int) {
	api := &api.Api{}
	api.New()

	db := db.GetInstance()

	from := start
	to := start.Add(time.Duration(span) * time.Minute)

	for to.Unix() < end.Unix() {

		fmt.Printf("%s %s %s (%d) から %s (%d) まで取得します。\n", service.Instrument, service.Granularity, from.Format("2006/01/02 15:04:05"), from.Unix(), to.Format("2006/01/02 15:04:05"), to.Unix())
		resp, err := api.GetCandles2(service.Instrument, service.Price, service.Granularity, strconv.FormatInt(from.Unix(), 10), strconv.FormatInt(to.Unix(), 10))
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Printf("%s %s %s (%d) %d 件の処理をおこないます。\n", service.Instrument, service.Granularity, from.Format("2006/01/02 15:04:05"), from.Unix(), len(resp.Candlestick))

		for i := 0; i < len(resp.Candlestick); i++ {
			candlestick := resp.Candlestick[i]

			// 存在してなければ行う
			exist := db.ExistInstrumentMatchCandlestick(resp.Instrument, resp.Granularity, candlestick.Time)
			if !exist {
				o, _ := strconv.ParseFloat(candlestick.Mid.Open, 64)
				c, _ := strconv.ParseFloat(candlestick.Mid.Close, 64)
				h, _ := strconv.ParseFloat(candlestick.Mid.High, 64)
				l, _ := strconv.ParseFloat(candlestick.Mid.Low, 64)

				// 陽線・陰線判定
				candleType := 0
				if o > c {
					candleType = -1
				} else if c > o {
					candleType = 1
				}

				// ロウソク足をinsert
				i := model.Instrument{
					Instrument:  resp.Instrument,
					Granularity: resp.Granularity,
					Time:        candlestick.Time,
					Volume:      candlestick.Volume,
					Open:        o,
					Close:       c,
					High:        h,
					Low:         l,
					Type:        candleType,
					Complete:    candlestick.Complete,
				}
				db.InsertInstrument(&i)
			}
		}
		from = to
		to = from.Add(time.Duration(span) * time.Minute)
	}

	c <- 999
}
