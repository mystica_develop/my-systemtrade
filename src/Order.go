package main

import (
	"db"
	"service"
	"time"
)

func main() {
	// ループ
	//	instruments := []string{"USD_JPY", "EUR_JPY", "EUR_USD"}
	instruments := []string{"USD_JPY", "EUR_JPY"}
	//	granularities := []string{"S5", "M5", "M15", "M30", "H1", "H4", "H8", "H12", "D"}
	granularities := []string{"S5", "M5"}

	db := db.GetInstance()
	defer db.Close()

	x := -1
	for x < 0 {
		// FIXME アカウント情報を更新

		for _, instrument := range instruments {
			for _, granularity := range granularities {
				getInstumentServiceByCount := &service.GetInstumentServiceByCount{Instrument: instrument, Granularity: granularity}
				getInstumentServiceByCount.Execute(100)
			}
			// FIXME 注文済みなら処理しない
			order2 := service.Order2Service{Instrument: instrument}
			order2.Execute()
		}

		x--
		time.Sleep(1 * time.Second) // 5秒休む
	}
}
